import java.time.LocalDate;

public class Car {
    private CarBrand marka;
    private CarType type;
    private double spalanie,wielkoscSilnika;
    private int iloscMiejsc,mocSilnika;
    private LocalDate dataWyprdukowania;
    private boolean automat;


    public CarDrive getNapend() {
        return napend;
    }

    public void setNapend(CarDrive napend) {
        this.napend = napend;
    }

    public Car(CarDrive napend) {
        this.napend = napend;
    }

    private CarDrive napend;
    public Car(CarBrand marka, CarType type, double spalanie, double wielkoscSilnika, int iloscMiejsc, int mocSilnika, LocalDate dataWyprdukowania, boolean automat, CarDrive naapend) {
        this.marka = marka;
        this.type = type;
        this.spalanie = spalanie;
        this.wielkoscSilnika = wielkoscSilnika;
        this.iloscMiejsc = iloscMiejsc;
        this.mocSilnika = mocSilnika;
        this.dataWyprdukowania = dataWyprdukowania;
        this.automat = automat;
        this.napend = napend;
    }
    public CarBrand getMarka() {
        return marka;
    }

    public void setMarka(CarBrand marka) {
        this.marka = marka;
    }

    public CarType getType() {
        return type;
    }

    public void setType(CarType type) {
        this.type = type;
    }

    public double getSpalanie() {
        return spalanie;
    }

    public void setSpalanie(double spalanie) {
        this.spalanie = spalanie;
    }

    public double getWielkoscSilnika() {
        return wielkoscSilnika;
    }

    public void setWielkoscSilnika(double wielkoscSilnika) {
        this.wielkoscSilnika = wielkoscSilnika;
    }

    public int getIloscMiejsc() {
        return iloscMiejsc;
    }

    public void setIloscMiejsc(int iloscMiejsc) {
        this.iloscMiejsc = iloscMiejsc;
    }

    public int getMocSilnika() {
        return mocSilnika;
    }

    public void setMocSilnika(int mocSilnika) {
        this.mocSilnika = mocSilnika;
    }

    public LocalDate getDataWyprdukowania() {
        return dataWyprdukowania;
    }

    public void setDataWyprdukowania(LocalDate dataWyprdukowania) {
        this.dataWyprdukowania = dataWyprdukowania;
    }

    public boolean isAutomat() {
        return automat;
    }

    public void setAutomat(boolean automat) {
        this.automat = automat;
    }
    @Override
    public String toString() {
        return "Car{" +
                "marka=" + marka +
                ", type=" + type +
                ", spalanie=" + spalanie +
                ", wielkoscSilnika=" + wielkoscSilnika +
                ", iloscMiejsc=" + iloscMiejsc +
                ", mocSilnika=" + mocSilnika +
                ", dataWyprdukowania=" + dataWyprdukowania +
                ", automat=" + automat +
                ", napend=" + napend +
                '}';
    }




}
\a.krause

