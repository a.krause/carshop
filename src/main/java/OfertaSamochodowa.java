public class OfertaSamochodowa {
    private Car samochód;

    public OfertaSamochodowa(Car samochód, int cena) {
        this.samochód = samochód;
        this.cena = cena;
    }

    private  int cena;

    public Car getSamochód() {
        return samochód;
    }

    public void setSamochód(Car samochód) {
        this.samochód = samochód;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    @Override
    public String toString() {
        return "OfertaSamochodowa{" +
                "samochód=" + samochód +
                ", cena=" + cena +
                '}';
    }
}
\a.krause