import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class CarShop {


    private String nazwa;
    private List<OfertaSamochodowa> listaOfert;

    public CarShop(String nazwa) {
        this.nazwa = nazwa;
        this.listaOfert = new ArrayList<>();
    }
    public List<CarBrand> dodawanieSamochodów(OfertaSamochodowa...ofertaSmochodowa){
        for (OfertaSamochodowa oferta:ofertaSmochodowa) {
            listaOfert.add(oferta);
            
        }
        List<CarBrand> markiSamoxhodów =new ArrayList<>();
        for (OfertaSamochodowa oferta:listaOfert) {
            markiSamoxhodów.add(oferta.getSamochód().getMarka());
            
        }
        return markiSamoxhodów;

    }
    public List<Car> samochodyNieStarszeNiż(String dataOdUżytkownika) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
        LocalDate date = LocalDate.parse(dataOdUżytkownika, formatter);
        List<Car> samochodyNieStarszeNiż = new ArrayList<>();
        for (OfertaSamochodowa oferta : listaOfert) {
            if (oferta.getSamochód().getDataWyprdukowania().isAfter(date)) {
                samochodyNieStarszeNiż.add(oferta.getSamochód());
            }


        }
        return samochodyNieStarszeNiż;
    }
    public List<Car>sanmochodyDanejMarki(String samochodyOdUżytkownika){
        List<Car>sanmochodyDanejMarki=new ArrayList<>();
        for (OfertaSamochodowa oferta: listaOfert) {
            if (oferta.getSamochód().getMarka().equals(CarBrand.valueOf(samochodyOdUżytkownika))){
                sanmochodyDanejMarki.add(oferta.getSamochód());
            }



        }

        return sanmochodyDanejMarki;
    }
    public List<Car>samochodyZnapędemNaCzteryKoła(String podajeUżytkownik){
        List<Car>samochodyZnapędemNaCzteryKoła=new ArrayList<>();
        for (OfertaSamochodowa oferta:listaOfert) {
            if (oferta.getSamochód().getMarka().equals(CarBrand.valueOf(podajeUżytkownik))){
                samochodyZnapędemNaCzteryKoła.add(oferta.getSamochód());
            }

        }
        return samochodyZnapędemNaCzteryKoła;
    }


    @Override
    public String toString() {
        return "CarShop{" +
                "nazwa='" + nazwa + '\'' +
                ", listaOfert=" + listaOfert +
                '}';
    }

}
\a.krause