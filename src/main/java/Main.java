import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Car car1=new Car(CarBrand.GAZ,CarType.SEDAN,12.0,1.6,6,1400, LocalDate.of(1969,8,31),false,CarDrive.PRZÓD);
        Car car2=new Car(CarBrand.VOLGSWAGEN,CarType.SEDAN,6.0,1.3,5,1300, LocalDate.of(1968,5,22),false,CarDrive.TYŁ);
        Car car3=new Car(CarBrand.VOLLVO,CarType.SEDAN,9.0,1.6,5,1500,LocalDate.of(1987,3,14),false,CarDrive.PRZÓD);
        Car car4=new Car(CarBrand.LADA,CarType.KOMBII,8.0,1.5,5,1500,LocalDate.of(1981,2,28),false,CarDrive.PRZÓD);
        Car car5=new Car(CarBrand.PORCHE,CarType.CABRIO,15.0,1.8,4,1900,LocalDate.of(2000,11,23),true,CarDrive.NA_WSZYSTKIE);
        OfertaSamochodowa oferta1=new OfertaSamochodowa(car1,25000);
        OfertaSamochodowa oferta2=new OfertaSamochodowa(car2,10000);
        OfertaSamochodowa oferta3=new OfertaSamochodowa(car3,5000);
        OfertaSamochodowa oferta4=new OfertaSamochodowa(car4,4000);
        OfertaSamochodowa oferta5=new OfertaSamochodowa(car5,32000);
        CarShop shop=new CarShop("Car Shop");
        shop.dodawanieSamochodów(oferta1,oferta2,oferta3,oferta4,oferta5);
        System.out.println(shop);
        System.out.println(shop.samochodyNieStarszeNiż("1967.06.20"));
        System.out.println(shop.sanmochodyDanejMarki("GAZ"));
        System.out.println(shop.samochodyZnapędemNaCzteryKoła("PORCHE"));



    }

}
\a.krause
